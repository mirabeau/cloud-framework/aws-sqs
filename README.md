AWS SQS
=======
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create SQS queues with Cloudformation. AWS resources that will be created are:
 * SQS queue

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x

Required python modules:
* boto
* boto3
* awscli

Dependencies
------------
 * None

Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
```

Role Defaults
-------------
```yaml
---
create_changeset   : True
debug              : False
cloudformation_tags: {}
tag_prefix         : "mcf"

aws_sqs_params:
  create_changeset: "{{ create_changeset }}"
  debug           : "{{ debug }}"
  slicename       : "{{ slicename | default(environment_abbr) }}"

  queues: {}
    ## Example queue configuration
    # queue1:
    #   queuename: "{{ slicename | default(environment_abbr) }}-default-queue1"
    # queue2:
    #   queuename: "{{ slicename | default(environment_abbr) }}-default-queue2"
    #   encrypt  : False

    #   deadletterqueue: True
    #   dlq: {}
    ##### The following options can be set for the dql but will be used with the defaults given below:
    #-------------------------------------------------------------------------------------------------
    #     fifoqueue             : "{{ queue.fifoqueue | default(false) }}"
    #     delayseconds          : "{{ queue.delayseconds | default(0) }}""
    #     maximummessagesize    : "{{ queue.maximummessagesize | default(262144) }}"
    #     messageretentionperiod: 1209600

    #  fifoqueue: True
    #  fifoq: {}
    ##### The following options can be set for the fifoq but will be used with the defaults given below:
    #-------------------------------------------------------------------------------------------------
    #   contentbaseddeduplication  : "{{ queue.contentbaseddeduplication | default(false) }}"

    ### The following options can be set but will be used with the defaults given below:
    #-----------------------------------------------------------------------------------
    #   fifoqueue             : false
    #   delayseconds          : 0
    #   maximummessagesize    : 262144
    #   messageretentionperiod: 345600

    ####### Only use if you want to encrypt your SQS queue
    #   kmsmasterkeyid              : "alias/aws/sqs"
    #   kmsdatakeyreuseperiodseconds: "300"
```

Example Playbook
----------------
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
    slicename       : "tst1"

    aws_sqs_params:
      queues:
        queue1:
          queuename: "{{ slicename | default(environment_abbr) }}-default-queue1"
        queue2:
          queuename: "{{ slicename | default(environment_abbr) }}-default-queue2"
          deadletterqueue: True
          dlq:
            delayseconds: 30

  roles:
    - aws-sqs
```
License
-------
GPLv3

Author Information
------------------
Christiaan Druif <cdruif@mirabau.nl>  
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
